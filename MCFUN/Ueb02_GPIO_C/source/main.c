/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         Exercise 02 - GPIO C
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          18.02.20018
 *
 *--------------------------------------------------------------------
 */
#include "platform.h"

#if !SOLUTION
#define TESTAT 1
#define AUFGABE 2
#define UEBUNG TESTAT

/**
 * Application entry point.
 */
int main(void) {
#if UEBUNG == AUFGABE
	uint32_t data;
	GPIOC->PDDR |= (1 << 8) | (1 << 9) | (1 << 10); //set output
	/* set PORTC 8,9,10 to GPIO PIN */
	PORTC->PCR[8] |= PORT_PCR_MUX(1);
	PORTC->PCR[9] |= PORT_PCR_MUX(1);
	PORTC->PCR[10] |= PORT_PCR_MUX(1);
	/* SET PORTB 1,2,3 Pull up and to GPIO */
	PORTB->PCR[1] |= PORT_PCR_PE(1) | PORT_PCR_PS(1) | PORT_PCR_MUX(1);
	PORTB->PCR[2] |= PORT_PCR_PE(1) | PORT_PCR_PS(1) | PORT_PCR_MUX(1);
	PORTB->PCR[3] |= PORT_PCR_PE(1) | PORT_PCR_PS(1) | PORT_PCR_MUX(1);


	for (;;) {
		data = GPIOB->PDIR;
		data <<= 7;
		GPIOC->PDOR = data;
	}

	// Never leave main
	for (;;) {
	}
#elif UEBUNG == TESTAT
	SIM->SCGC6 |= SIM_SCGC6_FTM3(1);
	FTM3->SC = FTM_SC_CLKS(2) |  FTM_SC_PS(3) | FTM_SC_CPWMS(0);
	PORTC->PCR[8] = PORT_PCR_MUX(3);
	PORTC->PCR[11] = PORT_PCR_MUX(3);
	FTM3->CONTROLS[4].CnSC = FTM_CnSC_MSB(1) | FTM_CnSC_ELSA(1);
	FTM3->CONTROLS[4].CnV = 10;
	FTM3->MOD = 100;
	FTM3->CONTROLS[7].CnSC = FTM_CnSC_MSB(1) | FTM_CnSC_ELSA(1);
	FTM3->CONTROLS[7].CnV = 100;
	// Never leave main
	for (;;) {
	}
#endif
}

#endif
