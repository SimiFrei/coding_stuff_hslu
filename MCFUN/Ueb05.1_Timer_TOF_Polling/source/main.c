/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         Exercise 05.1 - Poling, Interrupts & Timer
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          10.03.2020
 *
 *--------------------------------------------------------------------
 */
#include "platform.h"
#if !SOLUTION
#define POLLING 1
#define INTERRUPT 2
#define UEBUNG INTERRUPT
uint8_t ledstate = 1;
/**
 * Toggles PTC8 (Led green Front Left) every 500 ms
 */
#if UEBUNG == INTERRUPT
/* ISR */
void FTM0_IRQHandler(void){
	ledstate = ~ledstate;
	GPIOC->PDOR = ledstate << 8;
	FTM0->SC &= ~FTM_SC_TOF_MASK;
}
#endif
void main(void) {
	/* set PIN */
	PORTC->PCR[8] = PORT_PCR_MUX(1); //set as pin
	GPIOC->PDDR |= (1 << 8);
	/* enable Timer module */
	SIM->SCGC6 |= SIM_SCGC6_FTM0_MASK;
	/* set Timer */
	FTM0->SC |= FTM_SC_CLKS(2) | FTM_SC_PS(1);
#if UEBUNG == INTERRUPT
	FTM0->SC |= FTM_SC_TOIE(1);
#endif

	FTM0->MOD = 62500;
	/* set led */
	GPIOC->PDOR = ledstate << 8;

	/* Interrupt */
#if UEBUNG == INTERRUPT
	NVIC_SetPriority(FTM0_IRQn,8);
	NVIC_EnableIRQ(FTM0_IRQn);
#elif UEBUNG == POLLING
	while (true) {
		if (FTM0->SC & FTM_SC_TOF_MASK) {
			ledstate = ~ledstate;
			GPIOC->PDOR = ledstate << 8;
			FTM0->SC &= ~FTM_SC_TOF_MASK;
		}
	}
#endif

	// Never leave main
	for (;;) {
	}
}

#endif
