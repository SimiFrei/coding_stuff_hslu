/**
 * @file    sw07.c
 * @brief   Solution to homework SW07.
 * Diese Woche benötigte ich ca. 3h ausserhalb des Unterrichts.
 * Bei dieser Übung habe ich viel mit Pointer gearbeitet, dadurch konnte ich mein Wissen auffrischen.
 * Das Highlight war, als das Spielen mit den Pointern erfolgreich war und aktzeptable Ausgabe erschien,
 * mit plausiblen Werten. Bei der "Grafik Ausgabe" habe ich versucht alle Werte in die "bars" reinzurechnen.
 * Es ist etwas schwierig zu überprüfen, aber ich glaube es ist mir gelungen.
 */
#include "board.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define ZEITINTERVALL_MS 20

static const uint32_t size = 100;
static const uint8_t max_characters_per_line = 50;
static const uint8_t max_rows = 200;

void AnalyseSequence(float *avg, uint32_t *min, uint32_t *max, uint32_t *sum,
		uint32_t *data, uint32_t count);
void CalcGraphical(uint32_t *data, uint32_t **rows, uint32_t *largest_row,
		uint8_t *nbr_bars, uint32_t count);
void sortArray_ascending(uint32_t **data, uint32_t count);
float getMedian(uint32_t *data, uint32_t count);

int main(void) {
	uint32_t *data_sequence = NULL;
	uint32_t *bars = NULL;
	uint32_t *add_data_sequence_old = NULL;
	uint32_t count = 0; // count elements stored
	uint32_t min, max, sum, value, largest_row, marker, resize_factor;
	uint8_t nbr_bars;
	float median, avg;

	init();

	while (true) { /* run forever */

		while (!getSW1())
			/* wait until SW1 is activated */
			;
		resize_factor = 1;
		printf("start measurement\n");
		/* reserve storage for data of sequence */
		data_sequence = (uint32_t*) malloc(size * sizeof(uint32_t));
		bars = (uint32_t*) malloc(max_rows * sizeof(uint32_t)); //reserve storage, otherwise it could be that the graphic calculations can't be made
		if (data_sequence == NULL) {
			printf("measurement can't be started -> no storage!!\n");
			break;
		}
		while (getSW1()) { /* as long as SW is activated */
			/* measure */
			ADC1_Start();
			while (!ADC1_GetStatus())
				;
			value = ADC1_GetValue();
			marker = msTicks; //used to calculate time
			if (count < (size * resize_factor)) {
				data_sequence[count] = value;
				count++;
			} else { //if space isn't enough, realloc it
				add_data_sequence_old = data_sequence; //remember at which adress memory is stored
				data_sequence = (uint32_t*) realloc(data_sequence,
						sizeof(uint32_t) * (size * (resize_factor + 1)));
				if (data_sequence == NULL) {
					printf("Storage is full, the value is discarded\n");
					data_sequence = add_data_sequence_old; // get adress of "old memory block", right now, data_sequence == NULL
				} else {
					resize_factor++;
					data_sequence[count] = value;
					count++;
					printf("Storage extended\n");
				}
			}
			//wait till the next measurement should be done
			while (msTicks < (marker + ZEITINTERVALL_MS) && getSW1())
				;
		}

		printf("numerical results:\n");
		/* show result */
		AnalyseSequence(&avg, &min, &max, &sum, data_sequence, count);
		printf("Avg: %.2f, Min: %d, Max: %d\n", avg, min, max); //to print float remove CR_INTEGER_PRINTF" from the preprocessor settings
		printf("Number Measurements: %d, Sum: %d\n", count, sum);

		/* calc graphical result */
		CalcGraphical(data_sequence, &bars, &largest_row, &nbr_bars, count);
		/* show result */
		if (bars != NULL) { //if result has been calculated
			printf("graphical result:\n");
			for (uint8_t row = 0; row < nbr_bars; row++) {
				printf("\n%*c|",
						(max_characters_per_line * bars[row]) / largest_row,
						' ');
				if (bars[row] > 10000) {
					printf("%d", bars[row]);
				}
			}
		}

		/*median*/
		median = getMedian(data_sequence, count);
		printf("\nmedian: %.1f\n", median); //to print float remove CR_INTEGER_PRINTF" from the preprocessor settings
		/* keep it clean */
		count = 0;
		free(data_sequence);
		free(bars);
	}

	return 0;
}

void AnalyseSequence(float *avg, uint32_t *min, uint32_t *max, uint32_t *sum,
		uint32_t *data, uint32_t count) {
	///Get the Average, min-,max value and the sum of the values in the data pointer (array)///
	*avg = 0;
	*min = UINT32_MAX;
	*max = 0;
	*sum = 0;
	for (uint32_t i = 0; i < count; i++) {
		*sum += data[i]; // calc sum
		if (*max < data[i]) { //when actual max isn't max -> set new max
			*max = data[i];
		}
		if (*min > data[i]) { //when actual min isn't min -> set new min
			*min = data[i];
		}
	}
	*avg = (float)*sum / count;
}

void CalcGraphical(uint32_t *data, uint32_t **rows, uint32_t *largest_row,
		uint8_t *nbr_bars, uint32_t count) {
	///calc the values for the bar diagramm///
	uint32_t max_count = 0;
	uint32_t values_per_bar = 1;
	uint32_t value = 0;
	uint8_t diff_values_per_bar_count = 0;
	uint8_t row_count = 0;
	uint8_t val_count = 0;
	*nbr_bars = max_rows;
	*largest_row = 0;

	if (count > max_rows) {
		// when there are more rows than can be showed -> then there is more than one value in a single bar
		values_per_bar = count / max_rows;
		// when modulo isn't 0 -> than for some bars there is one more value
		diff_values_per_bar_count = count % max_rows;
	} else {
		*nbr_bars = count;
	}
	max_count = values_per_bar;
	if (diff_values_per_bar_count > 0) {
		max_count++;
		diff_values_per_bar_count--;
	}
	/* calculate values for the bars */
	for (uint32_t i = 0; i < count; i++) {
		value += data[i];
		val_count++;
		if (val_count >= max_count) {
			(*rows)[row_count] = value / max_count;
			if ((*rows)[row_count] > *largest_row) {
				*largest_row = (*rows)[row_count];
			}
			value = 0;
			row_count++;
			val_count = 0;
			max_count = values_per_bar;
			if (diff_values_per_bar_count > 0) {
				max_count++;
				diff_values_per_bar_count--;
			}
		}
	}
	/*for (uint8_t row = 0; row < *nbr_bars; row++) {
	 uint32_t value_bar = 0;
	 if (diff_values_per_bar_count > 0) {
	 max_count = values_per_bar + 1;
	 diff_values_per_bar_count--;
	 } else {
	 max_count = values_per_bar;
	 }
	 for (uint8_t i = 0; i < max_count; i++) {
	 value_bar += data[row * max_count + i];
	 }
	 (*rows)[row] = value_bar / max_count;
	 if ((*rows)[row] > *largest_row) {
	 *largest_row = (*rows)[row];
	 }
	 }*/

}

float getMedian(uint32_t *data, uint32_t count) {
	float median = 0;
	sortArray_ascending(&data, count);
	if (count % 2 == 0) {
		//even
		median = (float)(data[count / 2] + data[count / 2 + 1]) / 2;
	} else {
		//odd
		median = (float)data[count / 2 + 1];
	}
	return median;
}
void sortArray_ascending(uint32_t **data, uint32_t count) {
	for (uint32_t i = 0; i < count; i++) {
		for (uint32_t j = 0; j < count; j++) {
			if ((*data)[i] > (*data)[j]) { //when "i" val is greater than "j" val they have to be switched
				uint32_t tmp = (*data)[i];
				(*data)[i] = (*data)[j];
				(*data)[j] = tmp;
			}
		}
	}
}
