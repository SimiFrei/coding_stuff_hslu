/**
 * @file    sw03.c
 * @brief   Solution to homework SW03.
 */

#include "board.h"
#include "notes.h"
#include <stdio.h>
#include <stdbool.h>

#define SETBIT(byte,nbit)	((byte) |= (1<<(nbit)))
#define CLEARBIT(byte,nbit)	((byte) &= ~(1<<(nbit)))
#define CHECKBIT(byte,nbit)	((byte) & (1<<(nbit)))

#define READY	0
#define START	1
#define STOP	2

#define BUTTON2 0

void setNbrSEG(char p_nbr);
void clearSEG();
void tone(unsigned int frequency, unsigned long duration_ms);
void play_pirate();

int main(void) {
	char State = 0;
	char buttonstate = 0;
	char countdown = 10;
	int mill_counter = 0;
	init();
	setNbrSEG(countdown);

	while (countdown > 0) {
		switch (State) {
		case READY:
			if (getSW3()) { //wait till SW3 is pressed to start countdown
				State = START;
			}
			break;
		case START:
			if (getSW4() && !CHECKBIT(buttonstate, BUTTON2)) {
				//when SW3 is pressed SET LED and go to State STOP
				State = STOP;
				SETBIT(buttonstate, BUTTON2);
				setLED1();
				break;
			}
			if (!getSW4()) {
				CLEARBIT(buttonstate, BUTTON2);
			}
			delay_ms(1); //short delay, so a STOP signal from SW4 can be proceed fast
			mill_counter++;
			if (mill_counter >= 1000) { //After one second the countdown has to be decreased by one
				countdown--;
				printf("Counter: %d\n", countdown);
				setNbrSEG(countdown);
				mill_counter = 0;
			}
			break;
		case STOP:
			if (getSW4() && !CHECKBIT(buttonstate, BUTTON2)) {
				//when SW4 is pressed again the countdown should be resumed
				State = START;
				SETBIT(buttonstate, BUTTON2);
				clearLED1();
			}
			if (!getSW4()) {
				CLEARBIT(buttonstate, BUTTON2);
			}
			break;
		default:
			break;
		}
	}
	//when countdown elapsed play a funny song
	play_pirate();
	return 0;
}

void play_pirate() {
	int melody[] = { NOTE_A3, NOTE_C4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_E4,
	NOTE_F4, NOTE_F4, NOTE_F4, NOTE_G4, NOTE_E4, NOTE_E4, NOTE_D4, NOTE_C4,
	NOTE_C4, NOTE_D4, 0, NOTE_A3, NOTE_C4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_E4,
	NOTE_F4, NOTE_F4, NOTE_F4, NOTE_G4, NOTE_E4, NOTE_E4, NOTE_D4, NOTE_C4,
	NOTE_D4, 0, NOTE_A3, NOTE_C4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_E4,
	NOTE_F4, NOTE_F4, NOTE_F4, NOTE_G4, NOTE_G4, NOTE_G4, NOTE_A4, NOTE_AS4,
	NOTE_AS4, NOTE_A4, NOTE_G4, NOTE_A4, NOTE_D4, 0, NOTE_D4, NOTE_E4, NOTE_F4,
	NOTE_F4, NOTE_G4, NOTE_A4, NOTE_D4, 0, NOTE_D4, NOTE_F4, NOTE_E4, NOTE_E4,
	NOTE_F4, NOTE_D4, NOTE_E4 };

	int noteType[] = { 16, 16, 8, 8, 16, 16, 8, 8, 16, 16, 8, 8, 16, 16,
			16, 8, 16, 16, 16, 8, 8, 16, 16, 8, 8, 16, 16, 8, 8, 16, 16, 6, 8,
			16, 16, 8, 8, 16, 16, 8, 8, 16, 16, 8, 8, 16, 16, 16, 8, 16, 16, 16,
			8, 8, 8, 16, 8, 16, 16, 16, 8, 8, 16, 16, 4 };

	for (unsigned int thisNote = 0;
			thisNote < (sizeof(melody) / sizeof(melody[0])); thisNote++) {
		int noteDuration = 1000 / noteType[thisNote]; //calc type to ms
		tone(melody[thisNote], noteDuration);
		int pausebetweenNotes = noteDuration * 1.3; //1.3 is random factor which sounds good
		delay_ms(pausebetweenNotes);
	}
}
void tone(unsigned int frequency, unsigned long duration_ms) {
	if (frequency > 0) {
		uint32_t period_us = 1e6 / frequency; //1e6 cause its calculated in us
		uint32_t nbr_period = (duration_ms * 1000) / period_us; //how often the frequency should be played
		for (uint32_t i = 0; i < nbr_period; i++) {
			DAC0_setValue(0x00);
			delay_us(period_us / 2);
			DAC0_setValue(0xff);
			delay_us(period_us / 2);
		}
	} else {
		delay_ms(duration_ms);
	}

}
void setNbrSEG(char p_nbr) {
	clearSEG();
	//display the numbers on the 7-segment
	switch (p_nbr) {
	case 0:
		setSSEG2A();
		setSSEG2B();
		setSSEG2C();
		setSSEG2D();
		setSSEG2E();
		setSSEG2F();
		break;
	case 1:
		setSSEG2B();
		setSSEG2C();
		break;
	case 2:
		setSSEG2A();
		setSSEG2B();
		setSSEG2G();
		setSSEG2E();
		setSSEG2D();
		break;
	case 3:
		setSSEG2A();
		setSSEG2B();
		setSSEG2G();
		setSSEG2C();
		setSSEG2D();
		break;
	case 4:
		setSSEG2F();
		setSSEG2B();
		setSSEG2G();
		setSSEG2C();
		break;
	case 5:
		setSSEG2A();
		setSSEG2F();
		setSSEG2G();
		setSSEG2C();
		setSSEG2D();
		break;
	case 6:
		setSSEG2A();
		setSSEG2C();
		setSSEG2D();
		setSSEG2E();
		setSSEG2F();
		setSSEG2G();
		setSSEG2DP();
		break;
	case 7:
		setSSEG2A();
		setSSEG2B();
		setSSEG2C();
		break;
	case 8:
		setSSEG2A();
		setSSEG2B();
		setSSEG2C();
		setSSEG2D();
		setSSEG2E();
		setSSEG2F();
		setSSEG2G();
		break;
	case 9:
		setSSEG2A();
		setSSEG2B();
		setSSEG2C();
		setSSEG2D();
		setSSEG2DP();
		setSSEG2F();
		setSSEG2G();
		break;
	case 10:
		setSSEG1B();
		setSSEG1C();
		setSSEG2A();
		setSSEG2B();
		setSSEG2C();
		setSSEG2D();
		setSSEG2E();
		setSSEG2F();
		break;
	}
}
void clearSEG() {
	//clear the 7-Segment
	clearSSEG1A();
	clearSSEG1B();
	clearSSEG1C();
	clearSSEG1D();
	clearSSEG1E();
	clearSSEG1F();
	clearSSEG1G();
	clearSSEG1DP();

	clearSSEG2A();
	clearSSEG2B();
	clearSSEG2C();
	clearSSEG2D();
	clearSSEG2E();
	clearSSEG2F();
	clearSSEG2G();
	clearSSEG2DP();
}
