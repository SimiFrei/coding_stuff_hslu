/**
 * @file    sw08.c
 * @brief   Solution to homework SW08.
 * Übung Integral:
 * Die Funktionen geben korrekte Integralwerte zurück (mit TR überprüft). Die Funktionspointer waren
 * insgesamt neu für mich, somit konnte ich einiges lernen.
 * Übung Fibonacci:
 * Die Fibonacci-Funktion gibt die richtigen Werte (gem. Aufgabenstellung) zurück.
 * Ab einer gewissen grösse von n, dauert es sehr lange bis der Wert berechnet ist, da die
 * Funktion immer wieder sich selber aufruft. (Es entsteht bildlich gesprochen ein Baum, der durchläuft bis n < 2 ist.
 * Dabei gibt es desto mehr zweige je grösse n ist.)
 * Das wahre Problem ist jedoch, dass der Stack irgendwann zu klein ist, da jeweils die Funktionspointer (Rücksprungadresse)
 * auf den Stack geschrieben werden. Bei grossen n, sind es dann sehr viele Adressen und der Stack kommt an den Anschlag.
 * Die Tipps & Tricks waren an und für sich gut, allerdings wären sie eher letzte Woche hilfreich gewesen.
 * Diese Woche benötigte ich keine zusätzliche Zeit für die Lösung der Aufgaben.
 */
#include "board.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define INTEGRAL 0
#define FIBONACCI 1
#define UEBUNG FIBONACCI

#if UEBUNG == INTEGRAL
typedef enum {
	right, left
} riemansum_t;
typedef uint32_t (fun_t)(uint32_t);
fun_t square, double_plus_two, multiply_by_ten_subtract_five;
uint32_t integral(fun_t *fp, uint32_t start, uint32_t end, uint32_t stepsize);
uint32_t riemann_Sum(fun_t *fp, uint32_t start, uint32_t end, uint32_t stepsize,
		riemansum_t typeofsum);
#elif UEBUNG == FIBONACCI
uint32_t fibonacci(uint32_t n);
#endif
int main(void) {

	init();
#if UEBUNG == INTEGRAL
	uint32_t start = 3;
	uint32_t end = 6;
	uint32_t stepsize = 1;
	printf("integral f(x)=x^2, von 3 bis 6: %d\n",
			integral(square, start, end, stepsize));
	start = 1;
	end = 10;
	printf("integral f(x)=2*x+2, von 1 bis 10: %d\n",
			integral(double_plus_two, start, end, stepsize));
	start = 2;
	end = 7;
	printf("integral f(x)=10*x-5, von 2 bis 7: %d\n",
			integral(multiply_by_ten_subtract_five, start, end, stepsize));
#elif UEBUNG == FIBONACCI
	uint32_t result, n = 7;
	result = fibonacci(n);
	printf("Fibonacci(%d) = %d\n",n,result);
#endif

	return 0;
}

#if UEBUNG == INTEGRAL
uint32_t square(uint32_t x) {
	//f(x) = x^2
	return x * x;
}

uint32_t double_plus_two(uint32_t x) {
	// f(x) = 2*x-2
	return 2 * x + 2;
}

uint32_t multiply_by_ten_subtract_five(uint32_t x) {
	// f(x) = 10*x-5;
	if(x <= 0){
		return 0;
	}
	return 10 * x - 5;
}

uint32_t integral(fun_t *fp, uint32_t start, uint32_t end, uint32_t stepsize) {
	// calc integral with left and right riemann sum
	return (riemann_Sum(fp, start, end, stepsize, right)+riemann_Sum(fp, start, end, stepsize, left))/2;
}

uint32_t riemann_Sum(fun_t *fp, uint32_t start, uint32_t end, uint32_t stepsize,
		riemansum_t typeofsum) {
	//right -> start at "t0", left -> start at "t1"
	uint32_t sum = 0;
	if (typeofsum == right) {
		for (uint32_t i = start; i < end; i += stepsize) {
			sum += fp(i) * stepsize;
		}
	} else if (typeofsum == left) {
		for (uint32_t i = (start + 1); i <= end; i += stepsize) {
			sum += fp(i) * stepsize;
		}
	}
	return sum;
}
#elif UEBUNG == FIBONACCI
uint32_t fibonacci(uint32_t n){
	if(n < 2){
		return n;
	}
	return (fibonacci(n-1)+fibonacci(n-2));
}
#endif

