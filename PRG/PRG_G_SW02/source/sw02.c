 /**
 * @file    sw02.c
 * @brief   Solution to homework SW02.
 */

#include "board.h"
#include <stdio.h>

#define SETBIT(byte,nbit)	((byte) |= (1<<(nbit)))
#define CLEARBIT(byte,nbit)	((byte) &= ~(1<<(nbit)))
#define CHECKBIT(byte,nbit)	((byte) & (1<<(nbit)))
#define SWITCH1 0
#define SWITCH2 1
#define SWITCH3 0
#define SWITCH4 1
#define LED1 0
#define LED2 1

char readSwitch1and2(void);
char readButton1and2(void);
void blinkSequence(char led, char cycle);

int main(void) {

	init();
	int blinkcounter = 10;
	char blinkcycle = 0b00000000;
	char button = 0;
	char buttonold = 0;

	printf("Choose a binary value with SW 1 and 2, then press Button 1 or 2 and see magic happen! Have fun\n");
	while (blinkcounter > 0){
		button = readButton1and2();
		blinkcycle = readSwitch1and2();
		if(CHECKBIT(button,SWITCH3) && !CHECKBIT(buttonold,SWITCH3)){ //check if Switch 3 is now pressed and before released
			if(!CHECKBIT(button,SWITCH4)){ //check if Switch 4 is pressed
				blinkcounter--;
				printf("Button 1 pressed, Nbr of blinkcycle: %i, Nbr of sequences left: %i\n", blinkcycle, blinkcounter);
				blinkSequence(LED1, blinkcycle);
			}
		} else if (CHECKBIT(button,SWITCH4) && !CHECKBIT(buttonold,SWITCH4)){ //check if Switch 4 is now pressed and before released
			if(!CHECKBIT(button,SWITCH3)){ //check if Switch 3 is pressed
				blinkcounter--;
				printf("Button 2 pressed, Nbr of blinkcycle: %i, Nbr of sequences left: %i\n", blinkcycle, blinkcounter);
				blinkSequence(LED2, blinkcycle);
			}
		}
		buttonold = button;
	}
	printf("Blinksequence elapsed\n");
    return 0 ;
}

char readSwitch1and2(void){
	char State = 0;
	if(getSW1()){
		SETBIT(State,SWITCH1);
	}
	if(getSW2()){
		SETBIT(State,SWITCH2);
	}
	return State;
}

char readButton1and2(void){
	char buttonState = 0;
	if(getSW3()){
		SETBIT(buttonState,SWITCH3);
	}
	if(getSW4()){
		SETBIT(buttonState,SWITCH4);
	}
	return buttonState;
}

void blinkSequence(char led, char cycle){
	for (int i = 0; i < cycle; i++){
		switch(led){
			case LED1:
				setLED1();
				break;
			case LED2:
				setLED2();
				break;
		}
		delay_ms(500);
		switch(led){
			case LED1:
				clearLED1();
				break;
			case LED2:
				clearLED2();
				break;
		}
		delay_ms(500);
	}
}
