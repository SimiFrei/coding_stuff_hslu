/**
 * @file    sw04.c
 * @brief   Solution to homework SW04.
 */

#include "board.h"
#include <stdbool.h>
#include <stdio.h>

#define GETBIT(byte,nbit)	((byte>>(nbit)) & (0x01))

void clearSEG(void);
void setSEG(uint8_t p_value);

int main(void) {

	init();
	uint8_t n = 0;
	uint8_t res;
	uint8_t res4vB;
	do {
		/* compute code */
		res = n ^ (n / 2);
		/* print values to console -- version A*/
		printf("\n n : %3d => %1d%1d%1d%1d%1d%1d%1d%1d", n, GETBIT(res, 7),
				GETBIT(res, 6), GETBIT(res, 5), GETBIT(res, 4), GETBIT(res, 3),
				GETBIT(res, 2), GETBIT(res, 1), GETBIT(res, 0)); //Gray Code binary

		/* print values to console -- version B */
		printf("\n n : %3d => ", n);
		res4vB = res;
		for (int i = 0; i < 8; i++) {
			printf("%1d", (res4vB & 0x80) == 0 ? 0 : 1);
			res4vB <<= 1;
		}
		/* show values on 7-segment display */
		clearSEG();
		setSEG(res);
		delay_ms(500); /* optional */
		n = n + 1;
	} while (n != 0);

	return 0;
}


void setSEG(uint8_t p_val) {
	for (int i = 0; i < 8; i++) {
		switch (i) {
		case 0:
			if (GETBIT(p_val, i)) {
				setSSEG1A();
			}
			break;
		case 1:
			if (GETBIT(p_val, i)) {
				setSSEG1B();
			}
			break;
		case 2:
			if (GETBIT(p_val, i)) {
				setSSEG1C();
			}
			break;
		case 3:
			if (GETBIT(p_val, i)) {
				setSSEG1D();
			}
			break;
		case 4:
			if (GETBIT(p_val, i)) {
				setSSEG1E();
			}
			break;
		case 5:
			if (GETBIT(p_val, i)) {
				setSSEG1F();
			}
			break;
		case 6:
			if (GETBIT(p_val, i)) {
				setSSEG1G();
			}
			break;
		case 7:
			if (GETBIT(p_val, i)) {
				setSSEG1DP();
			}
			break;
		default:
			break;
		}
	}
}
void clearSEG(void) {
	//clear the 7-Segment
	clearSSEG1A();
	clearSSEG1B();
	clearSSEG1C();
	clearSSEG1D();
	clearSSEG1E();
	clearSSEG1F();
	clearSSEG1G();
	clearSSEG1DP();

	clearSSEG2A();
	clearSSEG2B();
	clearSSEG2C();
	clearSSEG2D();
	clearSSEG2E();
	clearSSEG2F();
	clearSSEG2G();
	clearSSEG2DP();
}
