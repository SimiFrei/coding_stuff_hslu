/**
 * @file    sw06.c
 * @brief   Solution to homework SW06.
 */

#include "board.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#define ANALYSE_OVER_DURATION 10

typedef enum {
	red, orange, yellow, green, turquouise, blue
} colors_t;

typedef enum {
	null, low, medium, high
} level_t;

typedef struct {
	uint8_t red;
	uint8_t green;
	uint8_t blue;
} rgb_t;

//for every "possible" color, the PWM duty cycle
rgb_t colors[6] = { [red] = { .red = 100, .green = 0, .blue = 0 }, [orange] = {
		.red = 100, .green = 50, .blue = 0 }, [yellow] = { .red = 100, .green =
		100, .blue = 0 }, [green] = { .red = 0, .green = 100, .blue = 0 },
		[turquouise] = { .red = 0, .green = 80, .blue = 80 }, [blue] = { .red =
				0, .green = 0, .blue = 100 } };

uint16_t getchanges_on_duration(uint32_t time_ms);
level_t getlevel(uint16_t changes);
colors_t getcolor(level_t plevels[ANALYSE_OVER_DURATION]);


int main(void) {
	uint16_t nbr_changes = 0;
	uint8_t index = 0; //used to write level to array
	colors_t m_color;
	level_t levels[ANALYSE_OVER_DURATION] = { null, null, null, null, null, null, null, null, null,
			null }; //used as ringbuffer
	uint32_t duration_activity_sample_ms = 1000; //1 second

	init();

	while (true) {
		//get the number of changes over 1 second
		nbr_changes = getchanges_on_duration(duration_activity_sample_ms);
		printf("Time %d [ms]\n", msTicks);
		printf("changes: %d\n", nbr_changes);
		//write level to array
		levels[index] = getlevel(nbr_changes);
		//set index for ringbuffer
		if (index >= 9) { 	//when at top go to start (0)
			index = 0;
		} else {		 	//go to the next index
			index++;
		}
		//get the color according to the levels over the defined duration (10s)
		m_color = getcolor(levels);
		printf("color: %d\n", m_color);
		//set RGB LED
		PWM_SetDutycycleRed(colors[m_color].red);
		PWM_SetDutycycleGreen(colors[m_color].green);
		PWM_SetDutycycleBlue(colors[m_color].blue);

	}

	return 0;
}

uint16_t getchanges_on_duration(uint32_t time_ms) {
	bool state = getTilt(); //get state
	uint16_t changes = 0;
	uint32_t marker = msTicks;
	while (msTicks < marker + time_ms) { //make for time_ms (1s)
		if (getTilt() != state) { //when state has changed
			changes++;
			state = !state;
		}
	}
	return changes;

}

level_t getlevel(uint16_t changes) {
	if (changes <= 9) {
		return null;
	} else if (changes <= 99) {
		return low;
	} else if (changes <= 199) {
		return medium;
	} else {
		return high;
	}

}

colors_t getcolor(level_t plevels[ANALYSE_OVER_DURATION]) {
	uint8_t activity[4] = { [null] = 0, [low] = 0, [medium] = 0, [high] = 0 }; //Get number of states
	for (uint8_t i = 0; i < 10; i++) {
		activity[plevels[i]] += 1;
	}
	//Evaluation of color
	if (activity[high] >= 6) {
		return blue;
	} else if (activity[medium] >= 6) {
		return turquouise;
	} else if (activity[medium] >= 3) {
		return green;
	} else if (activity[low] >= 6) {
		return yellow;
	} else if (activity[low] >= 3) {
		return orange;
	} else {
		return red;
	}
}
