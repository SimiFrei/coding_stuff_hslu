/*
 * main.c
 *
 *  Created on: 12 Nov 2020
 *      Author: simon
 */
#include "board.h"

#define BASIC 0
#define ERRORHANDLING 1
#define EXTENDED 2
#define UEBUNG EXTENDED
#if UEBUNG == BASIC
#include "stack_v1.h"
#elif UEBUNG == ERRORHANDLING
#include "stack_v2.h"
#elif UEBUNG == EXTENDED
#include "stack_v3.h"
#endif

int main(void) {
	init();

	stack_Initialize();
#if UEBUNG == BASIC
		stack_v1_test();
#elif UEBUNG == ERRORHANDLING
		stack_v2_test();
#elif UEBUNG == EXTENDED
		stack_v3_test();
#endif
	return 0;
}

