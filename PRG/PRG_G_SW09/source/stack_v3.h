/* this is your interface file for the stack, V1 */

/* 
Hints:
   - protect your header file against recursive includes
   - only put the needed things into the header
   - make it self-contained

Examples of a function declarations:
	extern void Push(int value);
	extern int Pop(void);

Example with #ifndef...#define...#endif:
	#ifndef MY_STACK_V1_H
	#define MY_STACK_V1_H
	<add your interface here>
	#endif
*/
#ifndef STACK_V3_H_
#define STACK_V3_H_

#include <stdint.h>
#include <stdbool.h>
typedef enum {
	STACK_OVERFLOW_ERROR,
	STACK_UNDERFLOW_ERROR,
	STACK_INIT_ERROR,
	STACK_NO_ERROR
} Stack_Error_t;
extern Stack_Error_t stack_v3_Initialize(uint8_t stacksize);
extern Stack_Error_t stack_v3_Deinit(void);
extern Stack_Error_t stack_v3_Push(uint8_t value);
extern Stack_Error_t stack_v3_Pop(uint8_t *value);
extern Stack_Error_t stack_v3_Peek(uint8_t *value);
extern bool stack_v3_isEmpty(void);
extern bool stack_v3_isFull(void);
extern bool stack_v3_isInit(void);
/*for testing*/
extern void stack_v3_test(void);
#endif
