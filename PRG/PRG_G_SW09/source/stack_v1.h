/* this is your interface file for the stack, V1 */

/* 
Hints:
   - protect your header file against recursive includes
   - only put the needed things into the header
   - make it self-contained

Examples of a function declarations:
	extern void Push(int value);
	extern int Pop(void);

Example with #ifndef...#define...#endif:
	#ifndef MY_STACK_V1_H
	#define MY_STACK_V1_H
	<add your interface here>
	#endif
*/
#ifndef STACK_V1_H_
#define STACK_V1_H_

#include <stdint.h>

extern void stack_Initialize(void);
extern void stack_Push(uint8_t value);
extern uint8_t stack_Pop();
extern uint8_t stack_Peek();
/*for testing*/
extern void stack_v1_test(void);
#endif
