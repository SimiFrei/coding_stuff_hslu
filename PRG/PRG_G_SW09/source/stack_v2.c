/* Stack_v2.c, implementing a more advanced stack */
#include "stack_v2.h"
#include <stdio.h>


typedef struct{
	uint8_t size;
	uint8_t *stack;
	uint8_t *stack_pointer;
} Stack_t;

static Stack_t m_stack = {.size=10, .stack = NULL, .stack_pointer = NULL};

Stack_Error_t stack_v2_Initialize(){
	m_stack.stack = (uint8_t*)malloc(m_stack.size);
	m_stack.stack_pointer = m_stack.stack; //start adress
	return STACK_NO_ERROR;
}

Stack_Error_t stack_v2_Push(uint8_t value){
	/* Check for Error */
	if(stack_v2_isFull()){
		return STACK_OVERFLOW_ERROR;
	}
	/* push value on stack */
	*m_stack.stack_pointer = value;
	m_stack.stack_pointer++;
	return STACK_NO_ERROR;
}

Stack_Error_t stack_v2_Pop(uint8_t *value){
	/* Check for Error */
	if(stack_v2_isEmpty()){
		return STACK_UNDERFLOW_ERROR;
	}
	/* pop value from stack */
	m_stack.stack_pointer--;
	*value = *(m_stack.stack_pointer);
	return STACK_NO_ERROR;
}
Stack_Error_t stack_v2_Peek(uint8_t *value){
	/* Check for Error */
	if(stack_v2_isEmpty()){
		return STACK_UNDERFLOW_ERROR;
	}
	*value = *(m_stack.stack_pointer-1);
	return STACK_NO_ERROR;
}

bool stack_v2_isEmpty(){
	if(m_stack.stack_pointer <= m_stack.stack){
		return true;
	}else{
		return false;
	}
}
bool stack_v2_isFull(){
	if(m_stack.stack_pointer >= (m_stack.stack + m_stack.size)){
		return true;
	}else{
		return false;
	}
}

/* Module Test function */
void stack_v2_test(void) {
	const uint8_t values[10] = {1,2,3,4,5,6,7,8,9,10};
	int i;
	uint8_t val;
	Stack_Error_t res;

	res = stack_v2_Initialize(); /* initialize stack */
	if (res!=STACK_NO_ERROR) {
		printf("Error in initialize\n");
		for (;;) {}/* error */
	}
	/* stack is empty here, test it */
	if (!stack_v2_isEmpty()) {
		printf("Stack is not Empty after initializing\n");
		for (;;) {}/* error */
	}
	/* push values on stack */
	for(i=0;i<sizeof(values)/sizeof(values[0]);i++) {
		res = stack_v2_Push(values[i]);
		if (res!=STACK_NO_ERROR) {
			printf("Error in push values @i=%d\n",i);
			for (;;) {}/* error */
		}
	}
	/* stack is full here, test it */
	if (!stack_v2_isFull()) {
		printf("Stack isn't full\n");
		for (;;) {}/* error */
	}
	/* stack must be full here: test error case */
	res = stack_v2_Push(123);
	if (res!=STACK_OVERFLOW_ERROR) {
		printf("Stack isn't full -> should get Overflow Error (0)/%d\n",res);
		for(;;){} /* error! */
	}
	/* get elements from the stack */
	for(i=sizeof(values)/sizeof(values[0])-1;i>=0;i--) {
		res = stack_v2_Peek(&val);
		if (res!=STACK_NO_ERROR || val!=values[i]) {
			printf("Error on Peek, or value on stack isn't same as value wrote in\n");
			for(;;){} /* error! */
		}
		res = stack_v2_Pop(&val);
		if (res!=STACK_NO_ERROR || val!=values[i]) {
			printf("Error on pop or value isn't correct\n");
			for(;;){} /* error! */
		}
	}
	/* stack is empty here, test it */
	if (!stack_v2_isEmpty()) {
		printf("stack isn't empty\n");
		for (;;) {}/* error */
	}
	/* test error cases: stack is empty here */
	res = stack_v2_Peek(&val);
	if (res!=STACK_UNDERFLOW_ERROR) {
		printf("no underflow on empty stack peek\n");
		for(;;){} /* error! */
	}
	res = stack_v2_Pop(&val);
	if (res!=STACK_UNDERFLOW_ERROR) {
		printf("no underflow on empty stack pop\n");
		for(;;){} /* error! */
	}
	printf("test v2 ended successful\n");
}


