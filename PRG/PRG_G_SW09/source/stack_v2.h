/* this is your interface file for the stack, V1 */

/* 
Hints:
   - protect your header file against recursive includes
   - only put the needed things into the header
   - make it self-contained

Examples of a function declarations:
	extern void Push(int value);
	extern int Pop(void);

Example with #ifndef...#define...#endif:
	#ifndef MY_STACK_V1_H
	#define MY_STACK_V1_H
	<add your interface here>
	#endif
*/
#ifndef STACK_V2_H_
#define STACK_V2_H_

#include <stdint.h>
#include <stdbool.h>
typedef enum {
	STACK_OVERFLOW_ERROR,
	STACK_UNDERFLOW_ERROR,
	STACK_NO_ERROR

} Stack_Error_t;
extern Stack_Error_t stack_v2_Initialize(void);
extern Stack_Error_t stack_v2_Push(uint8_t value);
extern Stack_Error_t stack_v2_Pop(uint8_t *value);
extern Stack_Error_t stack_v2_Peek(uint8_t *value);
extern bool stack_v2_isEmpty();
extern bool stack_v2_isFull();
/*for testing*/
extern void stack_v2_test(void);
#endif
