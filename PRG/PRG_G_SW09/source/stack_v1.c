/* this is your implemenation file for the stack, V1 */

/* Hints:
- include your own interface:
	#include "stack_v1.h"
- include all other needed headers (if any), e.g
	#include <stdint.h>
	#include <stdbool.h>
- example for a a stack structure:
	typedef struct {
		int stack[16];
		int *sp;
	} Stack_t;
	Define if the sp points to the next available element or to the last valid element on the stack.
	If sp points to the next free element, then a push operation could look like this:
	*sp = value;
	sp++;
*/
#include "stack_v1.h"
#include <stdio.h>

typedef struct {
	uint8_t size;
	uint8_t *Stack;
	uint8_t stackindex;
} Stack_t;

static Stack_t m_stack = {.size=10, .Stack=NULL, .stackindex=0};

void stack_Initialize(void){
	m_stack.Stack = (uint8_t*)malloc(m_stack.size);
	m_stack.stackindex = 0;
}

void stack_Push(uint8_t value){
	m_stack.Stack[m_stack.stackindex] = value;
	m_stack.stackindex++;
}
uint8_t stack_Pop(){
	m_stack.stackindex--;
	return m_stack.Stack[m_stack.stackindex];
}
uint8_t stack_Peek(){
	return m_stack.Stack[m_stack.stackindex-1];
}

/* Module Test function */
void stack_v1_test(void) {
	const uint8_t values[10] = {1,2,3,4,5,6,7,8,9,10};
	int i;
	stack_Initialize(); /* initialize stack */
	for(i=0;i<sizeof(values)/sizeof(values[0]);i++) {
		stack_Push(values[i]);
	}
	for(i=sizeof(values)/sizeof(values[0])-1;i>=0;i--) {
		if (stack_Peek()!=values[i]) {
			printf("Peek don't show element on top of stack!!\n"
					"i:%d, index:%d\n",i,m_stack.stackindex);
		}
		if (stack_Pop()!=values[i]) {
			printf("Pop don't show element on top of stack!!\n"
					"i:%d, index:%d\n",i,m_stack.stackindex);
		}
	}
	printf("test v1 ended successful\n");
}
