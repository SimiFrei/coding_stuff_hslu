/**
 * @file    sw05.c
 * @brief   Solution to homework SW05.
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "simulator.h"

/* maximal number of cars on bridge and island together -- do not change this */
const uint8_t max_cars = 10;
/* maximal number of cycle to switch the light */
const uint8_t max_cycle = 10;

/* sensors ml (mainland) il (island) in and out */
bool ml_out, il_out, ml_in, il_in = false;

/* traffic light mainland, island */
color_t ml, il = red;
typedef enum {
	START,
	MAINLAND_PASS,
	MAINLAND_TO_ISLAND_SWITCH,
	ISLAND_PASS,
	ISLAND_PASS_SWITCHABLE,
	ISLAND_TO_MAINLAND_SWITCH
} state_logic_t;
state_logic_t state = START;
uint8_t cars_on_br = 0; //Number of cars on the bridge
uint8_t cars_il = 0; //Number of cars on the island
/* variables for "fairness" */
uint8_t cycle_cnt = 0;
bool TimeElapsed = false; //indicates that the cycle cnt reached max
bool countTime = false; //indicates if cycle should be counted

void controller(void) {
	/* this function will be called (i.e. activated),
	 * each time a sensor changes its state. set
	 * traffic lights at mainland (ml) and island (il)
	 * such, that all requirements are met. sensors
	 * are set to 'true' if a car is LEAVING the corresponding
	 * area, set the signal back to 'false' to acknowledge
	 * the processing. */
	if (countTime) { // only count cycle when one Light is green
		cycle_cnt++;
		if (cycle_cnt >= max_cycle) {
			TimeElapsed = true;
			cycle_cnt = 0;
		}
	}else{
		cycle_cnt = 0;
	}
	/* State Machine -> set Lights at every State Change */
	switch (state) {
	case START:
		state = MAINLAND_PASS;
		countTime = true;
		ml = green;
		il = red;
		break;
	case MAINLAND_PASS:
		if (ml_out) { //car pass ml to il
			ml_out = false;
			cars_on_br++;
		}
		if (il_in) { //car arrived on il
			il_in = false;
			cars_on_br--;
			cars_il++;
		}
		//when car on il is ready to pass or max cars on il and br reached
		if (TimeElapsed || (cars_il + cars_on_br) >= max_cars) {
			state = MAINLAND_TO_ISLAND_SWITCH;
			countTime = false;
			ml = red;
			il = red;
		}
		break;
	case MAINLAND_TO_ISLAND_SWITCH:
		if (il_in) { //car arrived on il
			il_in = false;
			cars_on_br--;
			cars_il++;
		}
		//wait for no car on the bridge
		if (cars_on_br == 0) {
			state = ISLAND_PASS;
			TimeElapsed = false;
			ml = red;
			il = green;
		}
		break;
	case ISLAND_PASS:
		if (il_out) { //car pass il to ml
			il_out = false;
			cars_on_br++;
			cars_il--;
		}
		if (ml_in) { //car arrived ml
			ml_in = false;
			cars_on_br--;
		}
		//wait for number of cars on il / br lower than max
		if ((cars_il + cars_on_br) < max_cars) {
			state = ISLAND_PASS_SWITCHABLE;
			countTime = true;
			ml = red;
			il = green;
		}
		break;
	case ISLAND_PASS_SWITCHABLE:
		if (il_out) {
			il_out = false;
			cars_on_br++;
			cars_il--;
		}
		if (ml_in) {
			ml_in = false;
			cars_on_br--;
		}
		//car ready to pass from ml or no cars on il
		if (TimeElapsed || cars_il == 0) {
			state = ISLAND_TO_MAINLAND_SWITCH;
			countTime = false;
			ml = red;
			il = red;
		}
		break;
	case ISLAND_TO_MAINLAND_SWITCH:
		if (ml_in) {
			ml_in = false;
			cars_on_br--;
		}
		// Wait for no cars on the bridge
		if (cars_on_br == 0) {
			state = MAINLAND_PASS;
			TimeElapsed = false;
			ml = green;
			il = red;
		}
		break;
	}
}

