/* stack.c, implement a stack with dynamic memory allocation */
/* Bericht:
 * Ich konnte die Aufgabe innerhalb der Unterrichtslektionen lösen.
 * Das Programm funktioniert soweit und es werden alle Unittestcases bestanden,
 * bis auf den case wo Init(0) aufgerufen wird. Mein Programm gibt hier einen Error zurück.
 * Denn der Stack ist danach immer noch NULL. Was in meinen Augen auch mehr Sinn ergibt.
 * Denn wenn ein Stack der Grösse 0 initialisiert, kann nichts damit angefangen werden und ist somit eigentlich nicht initialisiert.
 * Aus diesen Gründen habe ich meinen Code so belassen wie er ist und nicht noch ein case hinzugefügt, welchen den Fall abfängt, dass die Stacksize <= 0 ist und dann kein Error zurück gibt.
 * Denn faktisch ist dass ja ein Fehler. Wenn dies explizit so vom Kunden erwünscht wäre, müsste einfach dieser case wie oben beschrieben
 * eingeführt werden und dann beispielsweise über die variable size überprüfen ob der stack initialisiert ist.
 *
 * Allgemein klappte diese Woche die Aufgabe gut, konnte meinen Code von letzter Woche praktisch übernehmen.
 * Ich fand es etwas schade, dass wir nicht wirklich darauf eingegangen sind, wie diese Unit tests erstellt werden.
 * Ich persönlich hätte dies interessanter gefunden, als nochmals die gleiche Übung wie letzte Woche zu machen, einfach
 * dass das .h file bereits vorgegeben war und man anhand von diesem die Funktionen implementieren musste.
 * Die Tipps & Tricks (Refactoring) kannte ich bereits, im Video war auch ziemlich ausführlich darauf eingegangen worden, dafür beinahe nichts über die Unittests.
 */

#include "stack.h"
#include <stdlib.h> /* for malloc() */
#include <stdbool.h>

typedef struct {
	size_t size;
	StackUnit_t *stack;
	StackUnit_t *stack_pointer;
	bool initialized;
} Stack_t;

Stack_t mStack = { .stack = NULL, .stack_pointer = NULL, .initialized=false };
/* Is stack is empty ? */
bool isEmpty(void) {
	return mStack.stack_pointer <= mStack.stack;
}

/* Is stack is full ? */
bool isFull(void) {
	return mStack.stack_pointer >= (mStack.stack + mStack.size);
}


/* Puts an element on top of the stack */
StackError_t Push(StackUnit_t newElement) {
	if (isFull()) {
		return STACK_OVERFLOW_ERROR;
	}
	*mStack.stack_pointer = newElement;
	mStack.stack_pointer++;
	return STACK_NO_ERROR;
}

/* Returns and removes the top element from the stack */
StackError_t Pop(StackUnit_t *stackElementp) {
	if (isEmpty()) {
		return STACK_UNDERFLOW_ERROR;
	}
	mStack.stack_pointer--;
	*stackElementp = *(mStack.stack_pointer);
	return STACK_NO_ERROR;
}

/* Returns top element from the stack without removing it */
StackError_t Peek(StackUnit_t *stackElementp) {
	if (isEmpty()) {
		return STACK_UNDERFLOW_ERROR;
	}
	*stackElementp = *(mStack.stack_pointer - 1);
	return STACK_NO_ERROR;
}

/* Initializes the stack */
StackError_t Init(size_t stackSize) {
	if(mStack.initialized){
		return STACK_REALLOC_ERROR;
	}
	mStack.size = stackSize;
	mStack.stack = (StackUnit_t*)malloc(mStack.size * sizeof(StackUnit_t));
	if (mStack.stack == NULL && mStack.size != 0) {
		return STACK_NO_MEMORY;
	}
	mStack.stack_pointer = mStack.stack;
	mStack.initialized = true;
	return STACK_NO_ERROR;
}

/* Deinitializes the stack, returns error code */
StackError_t DeInit(void) {
	if(!mStack.initialized){
		return STACK_NO_MEMORY;
	}
	free(mStack.stack);
	mStack.stack_pointer = NULL;
	mStack.size = 0;
	mStack.stack = NULL;
	mStack.initialized = false;
	return STACK_NO_ERROR;
}
